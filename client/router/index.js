import Vue from 'vue'
import Router from 'vue-router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import 'normalize.css'

import Home from '../views/Home'
import About from '../views/About'
import WhatEver from '../views/WhatEver'
import CardInfo from '../views/CardInfo'


Vue.use(Router)
Vue.use(ElementUI)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/About',
      component: About
    },
    {
      path: '/whatEver',
      component: WhatEver
    },
    {
      path: '/cardInfo/:id',
      component: CardInfo,
    },
  ]
})
