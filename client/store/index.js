import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex)

const store = new Vuex.Store({
    state:{
      apiLink: `http://0.0.0.0:3000/`,

      testResponse: {
        "success": true,
        "tiles": [
          {
            "id": 1,
            "type": "normal",
            "title": "Lorem Ipsum",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur elementum dolor in orci laoreet faucibus. Maecenas faucibus vulputate velit, eu rhoncus mi convallis non. Vestibulum et neque vel nisl convallis ultricies. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec accumsan ornare ipsum ac rutrum. Nulla odio urna, porta ut pretium eget, pharetra a leo. Aliquam pharetra tempus ex, vel sodales massa venenatis sit amet. Phasellus ut tortor egestas, placerat leo nec, mattis sapien. Praesent fermentum magna eu hendrerit dapibus. Duis mauris ex, pretium et faucibus non, facilisis at odio. Cras eget augue eros."
          },
          {
            "id": 2,
            "type": "normal",
            "title": "Lorem Ipsum",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur elementum dolor in orci laoreet faucibus. Maecenas faucibus vulputate velit, eu rhoncus mi convallis non. Vestibulum et neque vel nisl convallis ultricies. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec accumsan ornare ipsum ac rutrum. Nulla odio urna, porta ut pretium eget, pharetra a leo. Aliquam pharetra tempus ex, vel sodales massa venenatis sit amet. Phasellus ut tortor egestas, placerat leo nec, mattis sapien. Praesent fermentum magna eu hendrerit dapibus. Duis mauris ex, pretium et faucibus non, facilisis at odio. Cras eget augue eros."
          },
          {
            "id": 3,
            "type": "normal",
            "title": "Lorem Ipsum",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur elementum dolor in orci laoreet faucibus. Maecenas faucibus vulputate velit, eu rhoncus mi convallis non. Vestibulum et neque vel nisl convallis ultricies. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec accumsan ornare ipsum ac rutrum. Nulla odio urna, porta ut pretium eget, pharetra a leo. Aliquam pharetra tempus ex, vel sodales massa venenatis sit amet. Phasellus ut tortor egestas, placerat leo nec, mattis sapien. Praesent fermentum magna eu hendrerit dapibus. Duis mauris ex, pretium et faucibus non, facilisis at odio. Cras eget augue eros."
          },
          {
            "id": 4,
            "type": "double",
            "title": "Lorem Ipsum",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur elementum dolor in orci laoreet faucibus. Maecenas faucibus vulputate velit, eu rhoncus mi convallis non. Vestibulum et neque vel nisl convallis ultricies. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec accumsan ornare ipsum ac rutrum. Nulla odio urna, porta ut pretium eget, pharetra a leo. Aliquam pharetra tempus ex, vel sodales massa venenatis sit amet. Phasellus ut tortor egestas, placerat leo nec, mattis sapien. Praesent fermentum magna eu hendrerit dapibus. Duis mauris ex, pretium et faucibus non, facilisis at odio. Cras eget augue eros."
          },
          {
            "id": 5,
            "type": "normal",
            "title": "Lorem Ipsum",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur elementum dolor in orci laoreet faucibus. Maecenas faucibus vulputate velit, eu rhoncus mi convallis non. Vestibulum et neque vel nisl convallis ultricies. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec accumsan ornare ipsum ac rutrum. Nulla odio urna, porta ut pretium eget, pharetra a leo. Aliquam pharetra tempus ex, vel sodales massa venenatis sit amet. Phasellus ut tortor egestas, placerat leo nec, mattis sapien. Praesent fermentum magna eu hendrerit dapibus. Duis mauris ex, pretium et faucibus non, facilisis at odio. Cras eget augue eros."
          },
          {
            "id": 6,
            "type": "normal",
            "title": "Lorem Ipsum",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur elementum dolor in orci laoreet faucibus. Maecenas faucibus vulputate velit, eu rhoncus mi convallis non. Vestibulum et neque vel nisl convallis ultricies. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec accumsan ornare ipsum ac rutrum. Nulla odio urna, porta ut pretium eget, pharetra a leo. Aliquam pharetra tempus ex, vel sodales massa venenatis sit amet. Phasellus ut tortor egestas, placerat leo nec, mattis sapien. Praesent fermentum magna eu hendrerit dapibus. Duis mauris ex, pretium et faucibus non, facilisis at odio. Cras eget augue eros."
          },
          {
            "id": 7,
            "type": "double",
            "title": "Lorem Ipsum",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur elementum dolor in orci laoreet faucibus. Maecenas faucibus vulputate velit, eu rhoncus mi convallis non. Vestibulum et neque vel nisl convallis ultricies. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec accumsan ornare ipsum ac rutrum. Nulla odio urna, porta ut pretium eget, pharetra a leo. Aliquam pharetra tempus ex, vel sodales massa venenatis sit amet. Phasellus ut tortor egestas, placerat leo nec, mattis sapien. Praesent fermentum magna eu hendrerit dapibus. Duis mauris ex, pretium et faucibus non, facilisis at odio. Cras eget augue eros."
          },
          {
            "id": 8,
            "type": "normal",
            "title": "Lorem Ipsum",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur elementum dolor in orci laoreet faucibus. Maecenas faucibus vulputate velit, eu rhoncus mi convallis non. Vestibulum et neque vel nisl convallis ultricies. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec accumsan ornare ipsum ac rutrum. Nulla odio urna, porta ut pretium eget, pharetra a leo. Aliquam pharetra tempus ex, vel sodales massa venenatis sit amet. Phasellus ut tortor egestas, placerat leo nec, mattis sapien. Praesent fermentum magna eu hendrerit dapibus. Duis mauris ex, pretium et faucibus non, facilisis at odio. Cras eget augue eros."
          },
          {
            "id": 9,
            "type": "normal",
            "title": "Lorem Ipsum",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur elementum dolor in orci laoreet faucibus. Maecenas faucibus vulputate velit, eu rhoncus mi convallis non. Vestibulum et neque vel nisl convallis ultricies. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec accumsan ornare ipsum ac rutrum. Nulla odio urna, porta ut pretium eget, pharetra a leo. Aliquam pharetra tempus ex, vel sodales massa venenatis sit amet. Phasellus ut tortor egestas, placerat leo nec, mattis sapien. Praesent fermentum magna eu hendrerit dapibus. Duis mauris ex, pretium et faucibus non, facilisis at odio. Cras eget augue eros."
          }
        ]
      }
    },
    mutations:{

    },
    actions: {

      postRequest({commit}, {link, data, callback}) {
        axios.post(link, data)
        .then(res => {callback(res.data)})
        .catch(err => {console.log(err);})
      },

      getContent({commit}, {callback}){
        
        let data = store.state.testResponse;
        
        if (data.success) callback(data.tiles)
      },    

    }
})

export default store
