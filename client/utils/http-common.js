import axios from 'axios';

const baseURL = process.env.BASE_URL

export const HTTP = axios.create({
  baseURL: `${baseURL}/api/`,
  headers: {
    'crossDomain': true,
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
  }
})
