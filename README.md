
Создать одностраничное приложение vuejs по макету (proto.png)


Размер плитки подбирается исполнителем, но должен быть одинаковым для всех "нормальных" и соответствовать половине ширины двойной плитки. Подробности в разделе api.

Расстояние между плитками на усмотрение исполнителя.

Каждая плитка должна иметь картинку на заднем плане. Можно использовать или любой другой генератор картинок (например, https://placeimg.com/). Картинка должна быть отцентрирована и подобрана таким образом, чтобы всегда была видна "смысломая часть изображения"

Каждая плитка должна быть ссылкой или содержать ссылку на соответствующую статью, которая должна открываться по клику.

Текст плитки должен быть хорошо читаемым.

Верстка должна быть семантичной, кроссбраузерной, адаптивной

### Адаптивность

#### Desktop (viewport width > 1024px)
Плитки должны распологаться в 3 столбца

#### Tablet (viewport width <= 1024px)
Плитки должны распологаться в 2 столбца, плитка двойного размера занимает всю строку

#### Mobile (viewport width < 480px)
Плитки должны распологаться в один столбец шириной во всесь экран. При этом тип плитки игнорируется.

### API
Работа сервера зарыта мок объектом, содержащим ответ в формате json (tiles.json)
```json
[
  {
    "id": 1,
    "type": "normal",
    "title": "Lorem Ipsum",
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur elementum dolor in orci laoreet faucibus. Maecenas faucibus vulputate velit, eu rhoncus mi convallis non. Vestibulum et neque vel nisl convallis ultricies. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec accumsan ornare ipsum ac rutrum. Nulla odio urna, porta ut pretium eget, pharetra a leo. Aliquam pharetra tempus ex, vel sodales massa venenatis sit amet. Phasellus ut tortor egestas, placerat leo nec, mattis sapien. Praesent fermentum magna eu hendrerit dapibus. Duis mauris ex, pretium et faucibus non, facilisis at odio. Cras eget augue eros."
  }
];
```

__id__: {Numeric} Идентификатор

__type__: {('normal'|'double')} Тип плитки. Ширины плиток normal и double должны отличаться в 2 раза. Высоты при этом одинаковые и задаются в стилях исполнителем

__title__: {String} Заголовок, отображаемый в плитке и заглавии статьи

__description__: {String} Краткое описание статьи. Показывается только в плитке

__text__: {String} Полный текст статьи. Отображается только внутри статьи

Вы можете изменить содержание ответа, дополнить, но сохраняя общую структуру

### Инструментарий

Для успешного выполнения задания необходимо использовать:
- vue-cli
- vue
- vue-router
- бэм

Дополнительный плюс за использование vuex
